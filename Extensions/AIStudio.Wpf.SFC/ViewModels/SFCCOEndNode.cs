﻿using System;
using System.Collections.Generic;
using System.Text;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.SFC.ViewModels
{
    public class SFCCOEndNode : SFCNode
    {
        public SFCCOEndNode() : this(null)
        {
           
        }

        public SFCCOEndNode(IDiagramViewModel root) : base(root, SFCNodeKinds.COEnd)
        {
            ItemWidth = 280;
            ItemHeight = 10;

            ExecuteAddBottomOutput(null);
            ExecuteAddTopInput(null);
            ExecuteAddTopInput(null);
        }

        public SFCCOEndNode(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
        }

        public SFCCOEndNode(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override void ExecuteAddTopInput(object parameter)
        {
            FullyCreatedConnectorInfo connector = new FullyCreatedConnectorInfo(this, ConnectorOrientation.Top, true);
            connector.YRatio = 0;
            connector.XRatio = (40 + Input.Count * 200) / ItemWidth;
            Input.Add(Input.Count, connector);

            AddConnector(connector);
        }

        public override void ExecuteAddBottomOutput(object parameter)
        {
            FullyCreatedConnectorInfo connector = new FullyCreatedConnectorInfo(this, ConnectorOrientation.Bottom, true);
            connector.YRatio = 1;
            connector.XRatio = (40 + Output.Count * 200) / ItemWidth;
            Output.Add(Output.Count, connector);

            AddConnector(connector);
        }

    }
}
