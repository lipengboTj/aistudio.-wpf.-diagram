﻿using AIStudio.Wpf.DiagramDesigner;

namespace AIStudio.Wpf.Flowchart.ViewModels
{
    public class MiddleFlowNodeData : BindableBase
    {
        private string _title;
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                SetProperty(ref _title, value);
            }
        }

        public MiddleFlowNodeData()
        {
            Title = "审批";
        }

        private int _status = 100;
        public int Status
        {
            get
            {
                return _status;
            }
            set
            {
                SetProperty(ref _status, value);
            }
        }

        private string _remark = "同意";
        public string Remark
        {
            get
            {
                return _remark;
            }
            set
            {
                SetProperty(ref _remark, value);
            }
        }
    }
}
