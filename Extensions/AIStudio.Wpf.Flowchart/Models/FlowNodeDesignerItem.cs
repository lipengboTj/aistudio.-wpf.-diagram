﻿using AIStudio.Wpf.Flowchart.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Xml.Serialization;
using AIStudio.Wpf.DiagramDesigner;
using System.ComponentModel.Design;
using System.Reflection;

namespace AIStudio.Wpf.Flowchart.Models
{
    [Serializable]
    [XmlInclude(typeof(FlowNodeDesignerItem))]
    public class FlowNodeDesignerItem : DesignerItemBase
    {
        public FlowNodeDesignerItem()
        {

        }
        public FlowNodeDesignerItem(FlowNode item) : base(item)
        {
            if (item is MiddleFlowNode middleFlow)
            {
                UserIds = middleFlow.UserIds;
                RoleIds = middleFlow.RoleIds;
                ActType = middleFlow.ActType;
                SimulateApprove = middleFlow.SimulateApprove;           
            }
            Color = item.StatusColor;
            Kind = item.Kind;
            StateImage = item.StateImage;
            Status = item.Status;
            Remark = item.Remark;
        }

        [XmlArray]
        public List<string> UserIds
        {
            get; set;
        }

        [XmlArray]
        public List<string> RoleIds
        {
            get; set;
        }

        [XmlAttribute]
        public string ActType
        {
            get; set;
        }

        [XmlAttribute]
        public bool SimulateApprove
        {
            get; set;
        }

        [XmlAttribute]
        public int Status
        {
            get; set;
        }

        [XmlAttribute]
        public string Remark
        {
            get; set;
        }

        [XmlAttribute]
        public string Color
        {
            get; set;
        }

        [XmlAttribute]
        public NodeKinds Kind
        {
            get; set;
        }

        [XmlAttribute]
        public string StateImage
        {
            get; set;
        }
    }
}
