﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class SharpDrawingDesignerItemViewModel : DrawingDesignerItemViewModelBase
    {
        public SharpDrawingDesignerItemViewModel()
        {
        }

        public SharpDrawingDesignerItemViewModel(IDiagramViewModel root, DrawMode drawMode, Point startPoint, ColorViewModel colorViewModel, bool erasable) : base(root, drawMode, startPoint, colorViewModel, erasable)
        {
        }

        public SharpDrawingDesignerItemViewModel(IDiagramViewModel root, DrawMode drawMode, List<Point> points, ColorViewModel colorViewModel, bool erasable) : base(root, drawMode, points, colorViewModel, erasable)
        {

        }

        public SharpDrawingDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public SharpDrawingDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        protected override void InitNewDrawing()
        {
            if (IsFinish)
            {
                var path = GetPath();
                var rect = new Rect(Points[0], Points[1]);
                var point0 = rect.TopLeft;
                var point1 = rect.BottomRight;
                PathGeometry pathGeometry = PathGeometry.CreateFromGeometry(Geometry.Parse(path));
                var transformGroup = new TransformGroup();
                double radiox = Math.Abs(point1.X - point0.X) / pathGeometry.Bounds.Width;
                double radioy = Math.Abs(point1.Y - point0.Y) / pathGeometry.Bounds.Height;
                transformGroup.Children.Add(new TranslateTransform((point0.X) / radiox - pathGeometry.Bounds.Left, (point0.Y) / radioy - pathGeometry.Bounds.Top));
                transformGroup.Children.Add(new ScaleTransform(radiox, radioy));
                pathGeometry.Transform = transformGroup;
                Geometry = pathGeometry;
            }
            base.InitNewDrawing();
        }


        public override bool OnMouseMove(IInputElement sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var point = e.GetPosition(sender);
                if (Points == null || Points.Count == 0)//没有起始点
                {
                    return true;
                }

                if ((Points[0] - point).Length < ColorViewModel.LineWidth)
                {
                    return true;
                }

                if (Keyboard.IsKeyDown(Key.LeftShift))//正方形
                {
                    var len = Math.Abs(point.X - Points[0].X);//按X轴放大
                    point = new Point(Points[0].X + (point.X > Points[0].X ? len : -len), Points[0].Y + (point.Y > Points[0].Y ? len : -len));
                }

                if (Points.Count == 2)
                {
                    Points[1] = point;
                }
                else
                {
                    Points.Add(point);
                }

                var path = GetPath();
                var rect = new Rect(Points[0], Points[1]);
                var point0 = rect.TopLeft;
                var point1 = rect.BottomRight;

                PathGeometry pathGeometry = PathGeometry.CreateFromGeometry(Geometry.Parse(path));
                var transformGroup = new TransformGroup();
                double radiox = Math.Abs(point1.X - point0.X) / pathGeometry.Bounds.Width;
                double radioy = Math.Abs(point1.Y - point0.Y) / pathGeometry.Bounds.Height;
                transformGroup.Children.Add(new TranslateTransform((point0.X) / radiox - pathGeometry.Bounds.Left, (point0.Y) / radioy - pathGeometry.Bounds.Top));
                transformGroup.Children.Add(new ScaleTransform(radiox, radioy));
                pathGeometry.Transform = transformGroup;
                Geometry = pathGeometry;

                IsFinish = true;

                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool OnMouseDown(IInputElement sender, MouseButtonEventArgs e)
        {
            return true;
        }

        public override bool OnMouseUp(IInputElement sender, MouseButtonEventArgs e)
        {
            return base.OnMouseUp(sender, e);
        }

        private string GetPath()
        {
            string path = "";
            switch (DrawMode)
            {
                case DrawMode.ErasableTriangle: path = "M50,0 L100,100 0,100 z"; break;
                case DrawMode.ErasableRhombus: path = "M50,0 L100,100 50,200 0,100 z"; break;
                case DrawMode.ErasableHexagon: path = "M50,0 150,0 200,100 150,200 50,200 0,100 Z"; break;
                case DrawMode.ErasablePentagram: path = "M 12.8604,10.4421L 11,6.0593L 9.13781,10.4463L 4.39462,10.8613L 7.99148,13.988L 6.92042,18.6273L 11.0056,16.1727L 15.0868,18.625L 14.0147,13.9812L 17.6082,10.8575L 12.8604,10.4421 Z"; break;
                case DrawMode.ErasableStarFour: path = "M12,6.7L13.45,10.55L17.3,12L13.45,13.45L12,17.3L10.55,13.45L6.7,12L10.55,10.55L12,6.7"; break;
                case DrawMode.ErasableStarThree: path = "M12,9.5L13.2,13.5L16,16.5L12,15.6L7.9,16.5L10.7,13.5L12,9.5"; break;
                case DrawMode.ErasableChat: path = "M171.008 964.398545c-43.938909 0-70.306909-11.729455-76.288-33.931636-2.746182-10.216727-2.746182-30.045091 28.16-48.011636 24.855273-14.475636 47.569455-36.189091 67.677091-64.651636-117.946182-86.481455-185.134545-212.689455-185.134545-349.556364 0-249.693091 227.188364-452.864 506.414545-452.864 279.435636 0 506.740364 203.147636 506.740364 452.864 0 249.739636-227.328 452.933818-506.717091 452.933818-49.664 0-99.281455-6.702545-147.618909-19.921455C296.471273 947.688727 220.695273 964.398545 171.008 964.398545z"; break;
                case DrawMode.ErasableComment: path = "M928.078587 0h-838.26453C40.296574 0 0 40.296574 0 89.814057v598.760379c0 49.517483 40.296574 89.814057 89.814057 89.814057H119.752076v245.611507L400.450941 778.388493H928.078587c49.517483 0 89.814057-40.296574 89.814057-89.814057v-598.760379c0-49.517483-40.296574-89.814057-89.814057-89.814057z"; break;
                case DrawMode.ErasableCloud: path = "M224.219429 836.644571h531.858285c140.562286 0 245.138286-106.715429 245.138286-239.140571 0-136.283429-111.433143-236.141714-256.731429-236.141714-53.558857-105.874286-152.978286-174.006857-276.845714-174.006857-161.133714 0-293.997714 125.988571-308.132571 285.44-77.586286 22.272-136.722286 89.984-136.722286 180.425142 0 101.12 73.709714 183.405714 201.435429 183.405715z"; break;
                case DrawMode.ErasableArrowRight: path = "M11,16H3V8H11V2L21,12L11,22V16"; break;
                case DrawMode.ErasableArrowLeft: path = "M13,22L3,12L13,2V8H21V16H13V22"; break;
                case DrawMode.ErasableCheck: path = "M0,50 L30,100 100,0"; break;
                case DrawMode.ErasableClose: path = "M0,0 L100,100 M0,100 L100,0"; break;
                case DrawMode.ErasableHeart: path = "M510.68 883.15c-15.2 0-30.37-8.49-48.1-25.47-25.35-24.3-50.81-48.48-76.27-72.64-61.6-58.49-125.28-118.96-186.38-180.25-68.39-68.6-99.26-141.23-94.39-222.07 4.09-67.79 31.08-122.65 78.06-158.66 50.62-38.79 123.3-53.23 194.46-38.6 51.71 10.63 90 41.18 127.03 70.72l1.54 1.23c0.47 0.38 0.94 0.76 1.41 1.13 8.05-5.05 15.94-10.15 23.68-15.13 30.01-19.35 58.34-37.63 90.38-50.54 84.26-33.9 189.34-8.11 244.51 60.07 58.08 71.79 68.23 157.45 28.57 241.22-20 42.22-50.67 84.68-91.16 126.22-57.91 59.41-118.94 117.32-177.96 173.33-22.3 21.16-44.59 42.32-66.77 63.59-17.98 17.22-33.31 25.85-48.61 25.85z"; break;
            }

            return path;
        }
    }
}
