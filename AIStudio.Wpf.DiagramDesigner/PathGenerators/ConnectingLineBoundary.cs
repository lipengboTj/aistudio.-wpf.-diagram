﻿using System;
using System.Collections.Generic;
using System.Text;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class ConnectingLineBoundary : IPathGenerator
    {
        public PathGeneratorResult Get(IDiagramViewModel _, ConnectionViewModel link, PointBase[] route, PointBase source, PointBase target)
        {
            return PathGenerators.Boundary(_, link, route, source, target);
        }
    }
}
