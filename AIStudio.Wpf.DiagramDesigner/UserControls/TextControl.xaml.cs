﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIStudio.Wpf.DiagramDesigner
{
    /// <summary>
    /// TextControl.xaml 的交互逻辑
    /// </summary>
    public partial class TextControl : UserControl
    {
        public static readonly DependencyProperty AcceptsReturnProperty = DependencyProperty.Register(
           nameof(AcceptsReturn), typeof(bool), typeof(TextControl), new FrameworkPropertyMetadata(
           true));

        public bool AcceptsReturn
        {
            get => (bool)GetValue(AcceptsReturnProperty);
            set => SetValue(AcceptsReturnProperty, value);
        }

        public TextControl()
        {
            InitializeComponent();

            this.Loaded += TextControl_Loaded;
            this.PART_ShowText.IsVisibleChanged += PART_ShowText_IsVisibleChanged;
        }     

        private void TextControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= TextControl_Loaded;

            //新建后处于编辑状态，暂时关闭
            //PART_ShowText.Visibility = Visibility.Visible;
            //PART_TextBlock.Visibility = Visibility.Collapsed;
            //PART_ShowText.Focus();
            //if (!string.IsNullOrEmpty(PART_ShowText.Text))
            //{
            //    PART_ShowText.SelectionStart = PART_ShowText.Text.Length;
            //}

            if (this.DataContext is ISelectable selectable)
            {
                selectable.PropertyChanged -= TextControl_PropertyChanged;
                selectable.PropertyChanged += TextControl_PropertyChanged;
            }
            TextControl_PropertyChanged(this.DataContext, new System.ComponentModel.PropertyChangedEventArgs("IsSelected"));
        }

        private void PART_ShowText_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.DataContext is ISelectable selectable)
            {
                selectable.IsEditing = PART_ShowText.IsVisible;
                PART_ShowText.Focusable = PART_ShowText.IsVisible;
            }
        }

        private void TextControl_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is ISelectable selectable)
            {
                if (e.PropertyName == "IsSelected")
                {
                    if (selectable.InitIsEditing)
                    {
                        selectable.InitIsEditing = false;
                        PART_ShowText.Visibility = Visibility.Visible;
                        PART_TextBlock.Visibility = Visibility.Collapsed;
                        PART_ShowText.Focus();
                        if (!string.IsNullOrEmpty(PART_ShowText.Text))
                        {
                            PART_ShowText.SelectionStart = PART_ShowText.Text.Length;
                        }
                    }
                    else if (selectable.IsSelected == false)
                    {
                        BindingExpression binding = PART_ShowText.GetBindingExpression(TextBox.TextProperty);
                        binding.UpdateSource();

                        PART_ShowText.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                        PART_ShowText.Visibility = Visibility.Collapsed;
                        PART_TextBlock.Visibility = Visibility.Visible;                        
                        

                        selectable.IsEditing = false;
                    }
                }
                else if (e.PropertyName == "ShowText")
                {
                    PART_ShowText.Visibility = Visibility.Visible;
                    PART_TextBlock.Visibility = Visibility.Collapsed;
                    PART_ShowText.Focus();
                    if (!string.IsNullOrEmpty(PART_ShowText.Text))
                    {
                        PART_ShowText.SelectionStart = PART_ShowText.Text.Length;
                    }
                }                
            }
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (AcceptsReturn == false && e.Key == Key.Enter)
            {
                if (this.DataContext is DesignerItemViewModelBase designitem)
                {
                    designitem.ExitEditCommand.Execute(null);
                }
            }
            base.OnPreviewKeyDown(e);          
        }
 
    }

    public class ControlAttachProperty
    {
        #region WatermarkProperty 水印
        /// <summary>
        /// 水印
        /// </summary>
        public static readonly DependencyProperty WatermarkProperty = DependencyProperty.RegisterAttached(
            "Watermark", typeof(string), typeof(ControlAttachProperty), new FrameworkPropertyMetadata(""));

        public static string GetWatermark(DependencyObject d)
        {
            return (string)d.GetValue(WatermarkProperty);
        }

        public static void SetWatermark(DependencyObject obj, string value)
        {
            obj.SetValue(WatermarkProperty, value);
        }
        #endregion
    }
}
