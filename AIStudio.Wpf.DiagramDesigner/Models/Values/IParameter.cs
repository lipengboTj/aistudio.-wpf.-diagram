﻿namespace AIStudio.Wpf.DiagramDesigner
{
    public interface IParameter
    {
        string Text
        {
            get; set;
        }

        object Value
        {
            get; set;
        }

        void Add(object value);

    }
}
