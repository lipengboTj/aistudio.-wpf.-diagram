﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace AIStudio.Wpf.DiagramDesigner
{
    /// <summary>
    /// 连接线。中间点
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(ConnectorVertexItem))]
    public class ConnectorVertexItem : ConnectorPointItem
    {
        public ConnectorVertexItem()
        {

        }

        public ConnectorVertexItem(ConnectorVertexModel viewmodel) : base(viewmodel)
        {
            ConnectorVertexType = viewmodel.ConnectorVertexType;
        }

        [XmlAttribute]
        public ConnectorVertexType ConnectorVertexType
        {
            get; set;
        }
    }
}
