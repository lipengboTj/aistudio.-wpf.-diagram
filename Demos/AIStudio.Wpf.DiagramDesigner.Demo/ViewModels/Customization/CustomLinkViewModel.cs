﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class CustomLinkViewModel : BaseViewModel
    {
        public CustomLinkViewModel()
        {
            Title = "Custom link";
            Info = "Creating your own custom links is very easy!";
          
            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.DiagramOption.LayoutOption.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.DiagramOption.LayoutOption.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;

            DefaultDesignerItemViewModel node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 50, Text = "1" };
            DiagramViewModel.Add(node1);

            DefaultDesignerItemViewModel node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 300, Text = "2" };
            DiagramViewModel.Add(node2);

            DefaultDesignerItemViewModel node3 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 50, Text = "3" };
            DiagramViewModel.Add(node3);

            ConnectionViewModel connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector, DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            connector1.ColorViewModel.LineWidth = 6;
            connector1.ColorViewModel.LineColor.BrushType = BrushType.LinearGradientBrush;
            connector1.ColorViewModel.LineColor.GradientStop = new ObservableCollection<GradientStop>();
            connector1.ColorViewModel.LineColor.GradientStop.Add(new GradientStop(System.Windows.Media.Colors.Red, 0));
            connector1.ColorViewModel.LineColor.GradientStop.Add(new GradientStop(System.Windows.Media.Colors.Gray, 1)); 
            DiagramViewModel.Add(connector1);

            ConnectionViewModel connector2 = new ConnectionViewModel(DiagramViewModel, node2.RightConnector, node3.RightConnector, DrawMode.ConnectingLineStraight, RouterMode.RouterOrthogonal);
            connector2.ColorViewModel.LineWidth = 6;
            connector2.ColorViewModel.LineColor.Color = System.Windows.Media.Colors.Blue;
            DiagramViewModel.Add(connector2);
        }
    }
}
