﻿using System;
using System.Collections.Generic;
using System.Text;
using AIStudio.Wpf.Flowchart;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class FlowchartEditorViewModel : BaseViewModel
    {
        public FlowchartEditorViewModel()
        {
            Title = "FlowchartEditor";
            Info = "Encapsulated flowchart controls";

            GetDataCommand = new SimpleCommand(GetDataExcute);
            SetDataCommand = new SimpleCommand(SetDataExcute);
        }

        private List<SelectOption> _users = new List<SelectOption>()
        {
            new SelectOption(){ value = "操作员1",text = "操作员1" },
            new SelectOption(){ value = "操作员2",text = "操作员2" },
            new SelectOption(){ value = "Admin",text = "Admin" },
            new SelectOption(){ value = "Bob",text = "Bob" },
            new SelectOption(){ value = "Alice",text = "Alice" },
        };
        public List<SelectOption> Users
        {
            get
            {
                return _users;
            }
            set
            {
                _users = value;
            }
        }

        private List<SelectOption> _roles = new List<SelectOption>()
        {
            new SelectOption(){ value = "操作员",text = "操作员" },
            new SelectOption(){ value = "管理员",text = "管理员" },
            new SelectOption(){ value = "Admin",text = "Admin" },
        };
        public List<SelectOption> Roles
        {
            get
            {
                return _roles;
            }
            set
            {
                _roles = value;
            }
        }

        private Func<string> _getDataFunc;
        public Func<string> GetDataFunc
        {
            get
            {
                return _getDataFunc;
            }
            set
            {
                SetProperty(ref _getDataFunc, value);
            }
        }

        private string _inputData;
        public string InputData
        {
            get
            {
                return _inputData;
            }
            set
            {
                SetProperty(ref _inputData, value);
            }
        }

        private string _outputData;
        public string OutputData
        {
            get
            {
                return _outputData;
            }
            set
            {
                SetProperty(ref _outputData, value);
            }
        }

        private string _data = "{}";
        public string Data
        {
            get
            {
                return _data;
            }
            set
            {
                SetProperty(ref _data, value);
            }
        }


        public SimpleCommand GetDataCommand
        {
            get; private set;
        }

        public SimpleCommand SetDataCommand
        {
            get; private set;
        }

        private void GetDataExcute(object obj)
        {
            OutputData = GetDataFunc?.Invoke();
        }

        private void SetDataExcute(object obj)
        {
            Data = "{}";
            Data = InputData;
        }
    }
}
